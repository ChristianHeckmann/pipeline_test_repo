window.camWelcomeConf = {
    links: [
        {
            label: 'Angular.js Docs',
            href: 'https://docs.angularjs.org/api',
            description: 'Almost interesting'
        },
        {
            label: 'Camunda Docs',
            href: 'https://docs.camunda.org/manual/latest/',
            description: 'The Camunda BPM Manual'
        }, {
            label: 'UI Bootstrap',
            href: 'https://angular-ui.github.io/bootstrap/',
            description: 'Bootstrap components written in pure AngularJS by the AngularUI Team'
        }
    ]
};

