package de.ilume;

import java.util.Date;

public enum ProcessConstants {
    KATEGORIE("kategorie", String.class),
    QUELLE("quelle", String.class),
    HEADHUNTERWERTUNG("headHunterWertung", Integer.class),
    BEREICHPRIO1("bereichPrio1", String.class),
    BEREICHPRIO2("bereichPrio2", String.class),
    STATUS("status", String.class),
    ANGELEGTAM("angelegtAm", Date.class),
    DATENSCHUTZBESTIMMUNGEN("datenschutzbestimmungen", Boolean.class),
    ANREDE("anrede", String.class),
    VORNAME("vorname", String.class),
    NACHNAME("nachname", String.class),
    STRASSE("strasse", String.class),
    HAUSNUMMER("hausnummer", String.class),
    POSTLEITZAHL("postleitzahl", Integer.class),
    ORT("ort", String.class),
    TELEFON("telefon", String.class),
    HANDY("handy", String.class),
    EMAIL("eMail", String.class);

    private String name;
    private Class<?> clazz;

    ProcessConstants(final String name, Class<?> clazz) {
        this.name = name;
        this.clazz = clazz;
    }

    public String getName() {
        return name;
    }

    public Class<?> getClazz() {
        return clazz;
    }
}
