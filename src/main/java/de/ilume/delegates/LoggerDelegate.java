package de.ilume.delegates;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component("logger")
public class LoggerDelegate implements JavaDelegate {
    private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

    public void execute(DelegateExecution execution) {
        LOGGER.info(String.format("LoggerDelegate invoked by " +
                        "processDefinitionId=%s, " +
                        "activtyId=%s, " +
                        "activtyName='%s', " +
                        "processInstanceId=%s, " +
                        "businessKey=%s, " +
                        "executionId=%s",
                execution.getProcessDefinitionId(),
                execution.getCurrentActivityId(),
                execution.getCurrentActivityName(),
                execution.getProcessInstanceId(),
                execution.getProcessBusinessKey(),
                execution.getId()));

        execution.getVariables().entrySet().stream().map(e -> "key : " + e.getKey() + ",value " + e.getValue()).forEach(LOGGER::info);

        LOGGER.info("Logger finished");
    }
}
