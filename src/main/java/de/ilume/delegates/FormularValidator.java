package de.ilume.delegates;

import de.ilume.ProcessConstants;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.logging.Logger;

@Component("formularValidator")
public class FormularValidator implements JavaDelegate {
    private final Logger LOGGER = Logger.getLogger(FormularValidator.class.getName());

    @Override
    public void execute(DelegateExecution execution) {
        String kategorie = (String) execution.getVariable(ProcessConstants.KATEGORIE.getName());
        String quelle = (String) execution.getVariable(ProcessConstants.QUELLE.getName());
        Integer headHunterWertung = (Integer) execution.getVariable(ProcessConstants.HEADHUNTERWERTUNG.getName());
        String bereichPrio1 = (String) execution.getVariable(ProcessConstants.BEREICHPRIO1.getName());
        String bereichPrio2 = (String) execution.getVariable(ProcessConstants.BEREICHPRIO2.getName());
        String status = (String) execution.getVariable(ProcessConstants.STATUS.getName());
        Date angelegtAm = (Date) execution.getVariable(ProcessConstants.ANGELEGTAM.getName());
        Boolean datenschutzbestimmungen = (Boolean) execution.getVariable(ProcessConstants.DATENSCHUTZBESTIMMUNGEN.getName());
        String anrede = (String) execution.getVariable(ProcessConstants.ANREDE.getName());
        String vorname = (String) execution.getVariable(ProcessConstants.VORNAME.getName());
        String nachname = (String) execution.getVariable(ProcessConstants.NACHNAME.getName());
        String strasse = (String) execution.getVariable(ProcessConstants.STRASSE.getName());
        String hausnummer = (String) execution.getVariable(ProcessConstants.HAUSNUMMER.getName());
        Integer postleitzahl = (Integer) execution.getVariable(ProcessConstants.POSTLEITZAHL.getName());
        String ort = (String) execution.getVariable(ProcessConstants.ORT.getName());

        if (StringUtils.isEmpty(kategorie)) {
            throw new RuntimeException("Kategorie is empty");
        } else if (StringUtils.isEmpty(quelle)) {
            throw new RuntimeException("Quelle is empty");
        } else if (StringUtils.isEmpty(bereichPrio1)) {
            throw new RuntimeException("Bereich Prio 1 is empty");
        } else if (StringUtils.isEmpty(status)) {
            throw new RuntimeException("Status is empty");
        } else if (angelegtAm == null) {
            throw new RuntimeException("Angelegt am is empty");
        } else if (new Date().before(angelegtAm)) {
            throw new RuntimeException("Angelegt should not be in the future");
        } else if (datenschutzbestimmungen == null || !datenschutzbestimmungen) {
            throw new RuntimeException("Datenschutzbestimmungen zugestimmt is empty or false");
        } else if (StringUtils.isEmpty(anrede)) {
            throw new RuntimeException("Anrede is empty");
        } else if (StringUtils.isEmpty(vorname)) {
            throw new RuntimeException("Vorname is empty");
        } else if (StringUtils.isEmpty(nachname)) {
            throw new RuntimeException("Nachname is empty");
        } else if (StringUtils.isEmpty(strasse)) {
            throw new RuntimeException("Straẞe is empty");
        } else if (StringUtils.isEmpty(hausnummer)) {
            throw new RuntimeException("Hausnummer is empty");
        } else if (postleitzahl == null) {
            throw new RuntimeException("Postleitzahl is empty");
        } else if (postleitzahl > 99999) {
            throw new RuntimeException("Postleitzahl is invalid");
        } else if (StringUtils.isEmpty(ort)) {
            throw new RuntimeException("Ort is empty");
        }
    }
}
