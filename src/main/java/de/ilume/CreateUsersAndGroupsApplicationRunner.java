package de.ilume;

import com.camunda.consulting.util.UserGenerator;
import org.camunda.bpm.engine.ProcessEngine;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class CreateUsersAndGroupsApplicationRunner implements ApplicationRunner {
    private final ProcessEngine processEngine;

    public CreateUsersAndGroupsApplicationRunner(ProcessEngine processEngine) {
        this.processEngine = processEngine;
    }

    @Override
    public void run(ApplicationArguments args) {
        UserGenerator.addUser(processEngine, "AhnTu", "AhnTuIlume", "AhnTu", "Lam");
        UserGenerator.addUser(processEngine, "Patrick", "PatrickIlume", "Patrick", "Stahman");
        UserGenerator.addUser(processEngine, "Alexander", "AlexanderIlume", "Alexander", "Degenhardt");
        UserGenerator.addUser(processEngine, "Paul", "PaulIlume", "Paul", "Slowinski");
        UserGenerator.addUser(processEngine, "Anton", "AntonIlume", "Anton", "Wildermut");
        UserGenerator.addUser(processEngine, "PraneethKumar", "PraneethKumarIlume", "Praneeth Kumar", "Thiruveedhula");
    }

}