package de.ilume;

import de.ilume.delegates.FormularValidator;
import de.ilume.delegates.LoggerDelegate;
import org.aspectj.lang.annotation.Before;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class ProcessTest {
    private final Logger LOGGER = Logger.getLogger(ProcessTest.class.getName());

    @ClassRule
    public ProcessEngineRule processEngineRule = new ProcessEngineRule();

    @Before("value")
    public void before() {
        Mocks.reset();
        Mocks.register("logger", new JavaDelegate() {
            private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName() + "Mock");

            @Override
            public void execute(DelegateExecution execution) throws Exception {
                LOGGER.info("logger is executed");
            }
        });
        Mocks.register("formularValidator", new JavaDelegate() {
            final Logger LOGGER = Logger.getLogger(FormularValidator.class.getName() + "Mock");

            @Override
            public void execute(DelegateExecution execution) throws Exception {
                LOGGER.info("formularValidator is executed");
            }
        });
    }

    @Test
    @Deployment(resources = "recruiting.bpmn")
    public void test() {
        RuntimeService runtimeService = processEngineRule.getRuntimeService();

        Map<String, Object> variables = new HashMap<>();

        ProcessInstance processInstance = runtimeService
                .createProcessInstanceByKey("neuBewerbung")
                .businessKey("testBusinessKey")
                .setVariables(variables)
                .execute();

        BpmnAwareTests.assertThat(processInstance).isStarted();
        BpmnAwareTests.assertThat(processInstance).isActive();
        BpmnAwareTests.assertThat(processInstance).isNotEnded();

        Task task = BpmnAwareTests.task(processInstance);
        Assert.assertEquals("Daten anzeigen", task.getName());
        Assert.assertEquals("Activity_1gotapb", task.getTaskDefinitionKey());
        BpmnAwareTests.complete(task, variables);

        BpmnAwareTests.assertThat(processInstance).isEnded();
    }
}
