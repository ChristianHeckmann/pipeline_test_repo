package de.ilume.delegates;

import de.ilume.ProcessConstants;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.junit.Test;
import org.mockito.Mockito;

import java.text.SimpleDateFormat;

public class FormularValidatorTest {

    @Test
    public void test() throws Exception {
        DelegateExecution delegateExecution = Mockito.mock(DelegateExecution.class);
        Mockito.when(delegateExecution.getVariable(ProcessConstants.KATEGORIE.getName())).thenReturn("KATEGORIE");
        Mockito.when(delegateExecution.getVariable(ProcessConstants.QUELLE.getName())).thenReturn("QUELLE");
        Mockito.when(delegateExecution.getVariable(ProcessConstants.HEADHUNTERWERTUNG.getName())).thenReturn(3);
        Mockito.when(delegateExecution.getVariable(ProcessConstants.BEREICHPRIO1.getName())).thenReturn("BEREICHPRIO1");
        Mockito.when(delegateExecution.getVariable(ProcessConstants.BEREICHPRIO2.getName())).thenReturn("BEREICHPRIO2");
        Mockito.when(delegateExecution.getVariable(ProcessConstants.STATUS.getName())).thenReturn("STATUS");
        Mockito.when(delegateExecution.getVariable(ProcessConstants.ANGELEGTAM.getName())).thenReturn(new SimpleDateFormat("dd.MM.yyyy").parse("10.03.2020"));
        Mockito.when(delegateExecution.getVariable(ProcessConstants.DATENSCHUTZBESTIMMUNGEN.getName())).thenReturn(true);
        Mockito.when(delegateExecution.getVariable(ProcessConstants.ANREDE.getName())).thenReturn("ANREDE");
        Mockito.when(delegateExecution.getVariable(ProcessConstants.VORNAME.getName())).thenReturn("VORNAME");
        Mockito.when(delegateExecution.getVariable(ProcessConstants.NACHNAME.getName())).thenReturn("NACHNAME");
        Mockito.when(delegateExecution.getVariable(ProcessConstants.STRASSE.getName())).thenReturn("STRASSE");
        Mockito.when(delegateExecution.getVariable(ProcessConstants.HAUSNUMMER.getName())).thenReturn("88b");
        Mockito.when(delegateExecution.getVariable(ProcessConstants.POSTLEITZAHL.getName())).thenReturn(12345);
        Mockito.when(delegateExecution.getVariable(ProcessConstants.ORT.getName())).thenReturn("ORT");

        FormularValidator formularValidator = new FormularValidator();
        formularValidator.execute(delegateExecution);
    }
}
